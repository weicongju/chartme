module.exports = function(grunt)
{
  grunt.initConfig({
    cssmin: {
      compress: {
        files: {
          "distribute/public/css/chartme.min.css": [ "components/xcharts/build/xcharts.css",
                                          "src/public/css/chartme.css" ]
        }
      }
    },
    htmlmin: {
      dist: {
        options: {
          collapseWhitespace: true
        },
        files: {
          "distribute/index.html": "src/index.html"
        }
      }
    },
    concat: {
      dist: {
        src: [
            "components/d3/d3.min.js",
            "components/jquery/jquery.min.js",
            "components/xcharts/build/xcharts.min.js",
            "distribute/public/js/chartme.min.js",
        ],
        dest: "distribute/public/js/chartme.min.js"
      }
    },
    uglify: {
      chart: {
        files: {
          "distribute/public/js/chartme.min.js": [
           "src/public/js/chartme.js",
          ]
        }
      }
    },
    targethtml: {
      dist: {
        files: {
          "distribute/index.html": "distribute/index.html"
        }
      }
    }
  });

  grunt.loadNpmTasks("grunt-contrib-cssmin");
  grunt.loadNpmTasks("grunt-contrib-uglify");
  grunt.loadNpmTasks("grunt-contrib-concat");
  grunt.loadNpmTasks("grunt-contrib-htmlmin");
  grunt.loadNpmTasks("grunt-targethtml");
  
  grunt.registerTask("default", ["cssmin", "htmlmin", "uglify", "concat", "targethtml"]);
};
